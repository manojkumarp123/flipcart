## products

* name
* cost
* type
* brand
* image

##  rating
* product
* user
* integer 1-5

## users

## comments

* message
* product
* user

## orders

* payment
* product
* date
* user
* status
* feedback

### Goals

create product model

create api for product
*  list products
*  * products/

create rating model

* add rating for product
* * products/{product_id}/ratings/
* update rating
*  * products/{product_id}/ratings/{rating_id}/

create comment model

*  * add comment
*  * view comments
*  * modify comment
*  * delete comment
*  * product/{product_id}/comments/
*  * product/{product_id}/comments/{comment_id}/

create order model

*  * make a purchase
*  * orders/
*  * view order status
*  * orders/{order_id}/

### First Steps

* Create a Product model with fields name, cost, type, brand and image
* Create an endpoint to add a product
* Create an endpoint to list all products filtered by type
* Use django restframework's SimpleRouter, CreateModelMixin, ListModelMixin, GenericViewSet, ModelSerializer.